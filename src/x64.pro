QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += main.cpp balls.cpp
HEADERS += balls.h
FORMS += balls.ui
VERSION = 1.0.4
RESOURCES += res.qrc

win32 {
INCLUDEPATH += C:/OpenCV/include
LIBS += -LC:\OpenCV\x64
LIBS += -lopencv_core451 -lopencv_imgproc451
TARGET = ..\..\Balls_x64\Balls_x64
RC_ICONS += ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "Balls_x64"
QMAKE_TARGET_DESCRIPTION = "Balls_x64"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2021 Horuzhiy Ruslan"
}

unix {
INCLUDEPATH += /usr/local/include/opencv4 /usr/include/opencv4
LIBS += -lopencv_core -lopencv_imgproc
TARGET = ../linux-bin/Balls_x64
QMAKE_LFLAGS += -no-pie
RC_ICONS += ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "Balls_x64"
QMAKE_TARGET_DESCRIPTION = "Balls_x64"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2021 Horuzhiy Ruslan"
}

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

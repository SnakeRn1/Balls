#include "balls.h"
#include "ui_balls.h"

using namespace cv;
using namespace std;

Balls::Balls(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Balls)
{
    ui->setupUi(this);
    s = Size(800,600);

    QFile storage_file (":/bottle");
    storage_file.open(QIODevice::ReadOnly);
    cv::FileStorage storage_data(storage_file.readAll().data(),cv::FileStorage::MEMORY);
    if(storage_data.isOpened())storage_data["full_img"]>>banka_src;
    storage_file.close();
    storage_data.release();

    QFile balls_file (":/balls");
    balls_file.open(QIODevice::ReadOnly);
    cv::FileStorage balls_data(balls_file.readAll().data(),cv::FileStorage::MEMORY);
    if(balls_data.isOpened())
    {
        int c = 0;
        while(1)
        {
            c++;
            Mat img;
            cv::String s("_"+to_string(c));
            balls_data[s]>>img;
            if(img.empty()) break;
            else ball_vec.push_back(img);
        }
    }
    balls_file.close();
    balls_data.release();

/*
    Mat ball_img;
    int cnt = 0;
    cv::FileStorage storeWR("balls",cv::FileStorage::WRITE);
    while(1)
    {
        cnt++;
        ball_img= imread("balls_skins/ball ("+to_string(cnt)+").png",IMREAD_UNCHANGED);
        if(ball_img.empty()) break;
        extractChannel(ball_img,ball_img,3);
        cv::resize(ball_img,ball_img,Size(40,40));
        cv::String s("_"+to_string(cnt));
        storeWR<<s<<ball_img;
        //ball_vec.push_back(ball_img);
    }
    storeWR.release();
*/
    skin_now = -1;

/*
    QFile file_ball (":/ball");
    file_ball.open(QIODevice::ReadOnly);
    QByteArray read_data = file_ball.readAll();
    file_ball.close();
    vector<uchar> u_data (read_data.begin(),read_data.end());
    read_data.clear();
    ball_img = imdecode(u_data,IMREAD_UNCHANGED);
    u_data.clear();
    extractChannel(ball_img,ball_img,3);
    //cvtColor(ball_img,ball_img,COLOR_GRAY2BGR);
    cv::resize(ball_img,ball_img,Size(40,40));
    cv::FileStorage storeWR("ball",cv::FileStorage::WRITE);
    storeWR<<"ball"<<ball_img;
    storeWR.release();
*/

    if(banka_src.empty()) banka_src = Mat(Size(100,200),CV_8UC3,Scalar(0,0,0));
    else cvtColor(banka_src,banka_src,COLOR_GRAY2BGR);
    init_back();
    banks = 9;
    selected = -1;
    plus = true;
    move = false;
    global_cnt = 0;
    qt_img = QImage(s.width,s.height,QImage::Format_RGB888);
    frame_view = Mat(s,CV_8UC3,qt_img.bits());
    fon.copyTo(frame_view);
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(tim_func()));
    for (int t(0);t<100;t++)
    {
        int x = cv::randu<uint>()%800;
        int y = cv::randu<uint>()%400+200;
        int x_t = cv::randu<uint>()%750;
        int y_t = cv::randu<uint>()%400;
        draw_ball(Point(x,y));
        putText(frame_view,"Balls",Point(x_t,y_t),FONT_HERSHEY_PLAIN,2.0,get_color(static_cast<col>(cv::randu<uchar>()%7)));
    }
    pixmap = QPixmap::fromImage(qt_img.rgbSwapped());
    ui->pixmap->setPixmap(pixmap);

/*
    cv::FileStorage storeWR("storage_data",cv::FileStorage::WRITE);
    Mat gray;
    cv::resize(banka_src,banka_src,Size(100,200));
    cvtColor(banka_src,gray,COLOR_BGR2GRAY);
    storeWR<<"full_img"<<gray;
    storeWR.release();
*/
}

void Balls::init_back()
{
    int sq_size = 20;
    int v_sqrs = s.height/sq_size;
    int h_sqrs = s.width/sq_size;
    fon = cv::Mat(s,CV_8UC3,Scalar(0,0,0));
    for (int h(0);h<h_sqrs;++h)
    {
        int x = h*sq_size;
        for (int v(0);v<v_sqrs;++v)
        {
            int y = v*sq_size;
            int r = cv::randu<uint>()%30;
            rectangle(fon,Rect(x+2,y+2,sq_size-4,sq_size-4),Scalar(r,r,r),-1);
            r = cv::randu<uint>()%30;
            rectangle(fon,Rect(x+1,y+1,sq_size-2,sq_size-2),Scalar(r,r,r),1,LINE_AA);
        }
    }
}

void Balls::init_board(int banks)
{
    timer->stop();
    selected = -1;

    rects.clear();
    circles.clear();
    rects.resize(banks);
    circles.resize(banks);

    back_steps.clear();
    avail_backs = 5;
    ui->back->display(static_cast<int>(back_steps.size()));

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine e(seed);
    std::vector<col> foo;

    for (int p(0);p<banks-2;++p) for (int c(0);c<4;++c) foo.push_back(static_cast<col>(p));

    std::shuffle(foo.begin(), foo.end(), e);

    for (int p(0);p<banks-2;++p)
    {
        for (int c(0);c<4;++c)
        {
            circles.at(p).push_back(foo.at((p*4)+c));
        }
    }

    circles_backup = circles;
    rect_size = Size(s.width/(banks+3),200);
    dist = s.width/(banks+1);
    for (int ii(0);ii<banks;++ii)
    {
        int x_c = dist*(ii+1);
        rects.at(ii) = Rect(Point(x_c-(rect_size.width/2),300),rect_size);
    }
    cv::resize(banka_src,banka_full,rect_size);
    banka_simple = banka_full*0.2;
    banka_selected = banka_full*0.5;
    timer->start(10);
}

Scalar Balls::get_color(col color)
{
    int white = 255;
    int black = 0;
    switch (color)
    {
        case BLUE: return Scalar(white,black,black);
        case GREEN: return Scalar(black,white,black);
        case RED: return Scalar(black,black,white);
        case PINK: return Scalar(white,black,white);
        case SALAD: return Scalar(white,white,black);
        case YELLOW: return Scalar(black,white,white);
        case WHITE: return Scalar(white,white,white);
    default: return Scalar(100,100,100);
    }
}

void Balls::draw_ball(Point p, int ii, int pp)
{
    Scalar c;
    if((ii<0)||(pp<0)) c = Scalar(cv::randu<uint>()%255,cv::randu<uint>()%255,cv::randu<uint>()%255);
    else c = get_color((circles.at(ii).at(pp)));
    if(skin_now >= 0)
    {
        Rect r(p.x-20,p.y-20,40,40);
        if((Rect(Point(0,0),s).contains(r.tl()))&&(Rect(Point(0,0),s).contains(r.br())))
        {
            for (int z(0);z<3;++z)
            {
                if (c(z) > 0)
                {
                    Mat f;
                    extractChannel(frame_view(r),f,z);
                    double m = c(z)/255.0;
                    insertChannel(ball_vec.at(skin_now)*m+f,frame_view(r),z);
                }
            }
        }
    }
    else
    {
        Scalar s = c*0.7;
        Scalar w = get_color(WHITE);
        Point w_p = p + Point(2,-3);
        Point w_p2 = p + Point(10,-12);
        Point w_p3 = p + Point(6,-14);
        circle(frame_view,p,20,s,-1,LINE_AA);
        circle(frame_view,w_p,15,c,-1,LINE_AA);
        circle(frame_view,w_p2,2,w,-1,LINE_4);
        circle(frame_view,w_p3,2,w,-1,LINE_4);
    }
}

void Balls::tim_func()
{
    if(fon.empty()) frame_view.setTo(0);
    else fon.copyTo(frame_view);

    int goodok = 0;
    for (int bank(0);bank<banks;++bank)
    {
        int line = 1;
        bool full = check_full_rect(bank);
        int x_c = dist*(bank+1);
        if(full)
        {
            x_c += global_cnt%5;
            goodok++;
        }
        else if (bank == selected)
        {
            if(selected != -1) rectangle(frame_view,rects.at(bank),get_color(RED),line);
            frame_view(rects.at(bank)) += banka_selected;
        }
        else if((move)&&(rects.at(bank).contains(move_point)))
        {
            if(selected != -1) rectangle(frame_view,rects.at(bank),get_color(YELLOW),line);
            frame_view(rects.at(bank)) += banka_selected;
        }
        else
        {
            //if(selected != -1) rectangle(frame_view,rects.at(bank),get_color(BLUE),line);
            frame_view(rects.at(bank)) += banka_simple;
        }
        for (size_t ball(0);ball<circles.at(bank).size();++ball)
        {
            Point point(x_c,475-(ball*50));
            if((bank == selected)&&(circles.at(bank).size()-1 == ball)) //Словленный мяч
            {
                if(move)
                {
                    int click = check_rect_cont_click(move_point);
                    if(click != -1) draw_ball(Point(dist*(click+1),250-global_cnt), bank, ball);
                    draw_ball(move_point, bank, ball);
                }
                else
                {
                    point.y -= (20+global_cnt);
                    draw_ball(point, bank, ball);
                }
            }
            else draw_ball(point, bank, ball);
        }
        if(full) frame_view(rects.at(bank)+Point(global_cnt%5,0)) += banka_full;
    }

    if (goodok == (banks-2))
    {
        timer->stop();
        for (int t(0);t<50;t++)
        {
            int x = cv::randu<uint>()%800;
            int y = cv::randu<uint>()%400+200;
            int x_t = cv::randu<uint>()%750;
            draw_ball(Point(x,y));
            putText(frame_view,"WIN!!!",Point(x_t-50,y),FONT_HERSHEY_PLAIN,2.0,get_color(static_cast<col>(cv::randu<uchar>()%7)));
        }
        putText(frame_view,"WIN!!! WIN!!!",Point(s.width/2-300,100),FONT_HERSHEY_DUPLEX,3.0,Scalar(255,255,255),8);
        pixmap = QPixmap::fromImage(qt_img.rgbSwapped());
        ui->pixmap->setPixmap(pixmap);
        if(++banks>9) banks = 9;
        ui->diff->display(banks);
        skin_now = cv::randu<uint>()%ball_vec.size();
    }
    else putText(frame_view,"BALLS",Point(s.width/2-150,100),FONT_HERSHEY_TRIPLEX,3.0,Scalar(0,255-abs(global_cnt)*10,abs(global_cnt)*10));

    if(plus) global_cnt++;
    else global_cnt--;
    if(abs(global_cnt) >= 20) plus = !plus;

    pixmap = QPixmap::fromImage(qt_img.rgbSwapped());
    ui->pixmap->setPixmap(pixmap);
}

bool Balls::check_full_rect(int ii)
{
    if (      (circles.at(ii).size() == 4)
            &&(circles.at(ii).front() == circles.at(ii).at(1))
            &&(circles.at(ii).front() == circles.at(ii).at(2))
            &&(circles.at(ii).front() == circles.at(ii).at(3)))
        return true;
    else return false;
}

int Balls::check_rect_cont_click (Point p)
{
    int r = -1;
    for (int ii(0);ii<banks;++ii)
    {
        if(rects.at(ii).contains(p)) r = ii;
    }
    return r;
}

void Balls::mousePressEvent(QMouseEvent *event)
{
    if (timer->isActive())
    {
        int cursor_rect = check_rect_cont_click(cv::Point(event->x(),event->y()));
        if ((cursor_rect != -1)&&(!check_full_rect(cursor_rect)))
        {
            if ((selected != -1)&&(cursor_rect != selected))
            {
                move_ball(selected,cursor_rect);
                selected = -1;
            }
            else
            {
                if(!circles.at(cursor_rect).empty())
                {
                    global_cnt = -19;
                    selected = cursor_rect;
                }
            }
        }
        else
        {
            init_back();
            selected = -1;
        }
    }
    else on_btn_reset_clicked();
}

void Balls::on_btn_reset_clicked()
{
    banks = ui->diff->intValue();
    init_back();
    init_board(banks);
}

void Balls::on_btn_restart_clicked()
{
    circles = circles_backup;
    back_steps.clear();
    avail_backs = 5;
}

void Balls::on_btn_return_clicked()
{
    if(!back_steps.empty())
    {
        int now = back_steps.back()[1];
        int prev = back_steps.back()[0];
        circles.at(now).push_back(circles.at(prev).back());
        circles.at(prev).pop_back();
        back_steps.pop_back();
        ui->back->display(static_cast<int>(back_steps.size()));
        avail_backs--;
    }
}

void Balls::mouseReleaseEvent(QMouseEvent *event)
{
    move = false;
    if (timer->isActive()&&(selected != -1))
    {
        int cursor_rect = check_rect_cont_click(cv::Point(event->x(),event->y()));
        if ((cursor_rect != -1)&&(!check_full_rect(cursor_rect))&&(cursor_rect != selected))
        {
            move_ball(selected,cursor_rect);
            selected = -1;
        }
        else if (cursor_rect == -1)  selected = -1;
    }
}

void Balls::mouseMoveEvent(QMouseEvent *event)
{
    move = true;
    move_point = Point(event->x(),event->y());
}

void Balls::wheelEvent(QWheelEvent *event)
{
    if(event->delta()<0)
    {
        if(skin_now>-1) skin_now--;
    }
    else
    {
        if(skin_now<static_cast<int>(ball_vec.size()-1)) skin_now++;
    }
}

void Balls::move_ball(int prev,int now)
{
    if ((circles.at(prev).size() >= 1)&&(!check_full_rect(prev))&&(circles.at(now).size() < 4))
    {
        if (circles.at(now).empty())
        {
            circles.at(now).push_back(circles.at(prev).back());
            circles.at(prev).pop_back();
            back_add(prev,now);
        }
        else
        {
            if (circles.at(prev).back() == circles.at(now).back())
            {
                circles.at(now).push_back(circles.at(prev).back());
                circles.at(prev).pop_back();
                back_add(prev,now);
            }
        }
    }
}

void Balls::back_add(int prev,int now)
{
    back_steps.push_back(Vec2i(now,prev));
    if (back_steps.size()>avail_backs) back_steps.erase(back_steps.begin());
    ui->back->display(static_cast<int>(back_steps.size()));
}

Balls::~Balls()
{
    delete ui;
}

void Balls::on_diff_p_clicked()
{
    int val = ui->diff->intValue();
    if (val<9) val++;
    ui->diff->display(val);
}

void Balls::on_diff_n_clicked()
{
    int val = ui->diff->intValue();
    if (val>4) val--;
    ui->diff->display(val);
}

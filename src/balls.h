#ifndef BALLS_H
#define BALLS_H

#include <QMainWindow>
#include <QApplication>
#include <QImage>
#include <QPixmap>
#include <QTimer>
#include <QMouseEvent>
#include <QWheelEvent>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc.hpp"
#include <vector>
#include <random>
#include <iostream>

enum col {BLUE,GREEN,RED,PINK,SALAD,YELLOW,WHITE};

QT_BEGIN_NAMESPACE
namespace Ui { class Balls; }
QT_END_NAMESPACE

class Balls : public QMainWindow
{
    Q_OBJECT

public:
    Balls(QWidget *parent = nullptr);
    ~Balls();

private slots:
    void tim_func();

    void on_btn_reset_clicked();

    void on_btn_restart_clicked();

    void on_btn_return_clicked();

    void on_diff_p_clicked();

    void on_diff_n_clicked();

private:
    int global_cnt;
    int banks;
    int selected;
    int dist;
    size_t avail_backs;
    bool plus;
    bool move;
    cv::Point move_point;
    cv::Size s;
    Ui::Balls *ui;
    QImage qt_img;
    cv::Mat frame_view;
    std::vector<cv::Mat> ball_vec;
    QTimer* timer;
    QPixmap pixmap;
    std::vector<cv::Rect> rects;
    std::vector<std::vector<col>> circles;
    std::vector<std::vector<col>> circles_backup;
    cv::Mat banka_src,banka_full,banka_selected,banka_simple;
    cv::Mat fon;
    cv::Size rect_size;
    std::vector<cv::Vec2i> back_steps;

    int skin_now;
    void init_board(int);
    void move_ball(int,int);
    bool check_full_rect(int);
    int check_rect_cont_click(cv::Point p);
    cv::Scalar get_color(col);
    void draw_ball(cv::Point, int ii = -1, int pp = -1);
    void init_back();
    void back_add(int prev, int now);

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
};
#endif // BALLS_H
